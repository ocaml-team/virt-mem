(* Memory info for virtual domains.
   (C) Copyright 2008 Richard W.M. Jones, Red Hat Inc.
   http://libvirt.org/

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)

open Printf

open Virt_mem_gettext.Gettext
open Virt_mem_utils
open Virt_mem_types
open Virt_mem_mmap

let run debug ({ domname = domname; mem = mem }, ksymmap, _) =
  try
    (* I don't know why but this symbol doesn't exist in 2.6.9
     * even in kallsyms.  Hence this won't work with that kernel.
     * It's possible we can fall back to memory scanning. XXX
     *)
    let log_buf = Ksymmap.find "log_buf" ksymmap in
    let log_buf = follow_pointer mem log_buf in
    let log_buf_len = Ksymmap.find "log_buf_len" ksymmap in
    let log_buf_len = Int64.of_int32 (get_C_int mem log_buf_len) in
    (*	let log_start = Ksymmap.find "log_start" ksymmap in
	let log_start = get_C_long mem log_start in *)
    let log_end = Ksymmap.find "log_end" ksymmap in
    let log_end = get_C_long mem log_end in
    (*	let con_start = Ksymmap.find "con_start" ksymmap in
	let con_start = get_C_long mem con_start in *)
    let logged_chars = Ksymmap.find "logged_chars" ksymmap in
    let logged_chars = get_C_long mem logged_chars in

    (* This is basically the same algorithm from printk.c:do_syslog
     * type=3, translated into OCaml.  Unlike the kernel version
     * however we don't copy the buffer backwards.
     *)
    let get_log_buf idx =
      let addr = log_buf +^ (idx &^ (log_buf_len -^ 1L)) in
      Char.chr (get_byte mem addr)
    in

    let count = log_buf_len in
    let count = if count > logged_chars then logged_chars else count in
    let limit = log_end in

    let rec loop i =
      if i >= 0L then (
	let j = limit-^1L-^i in
	if j +^ log_buf_len >= log_end then (
	  let c = get_log_buf j in
	  printf "%c" c;
	  loop (i-^1L)
	)
      )
    in
    loop (count-^1L)
  with
    Not_found ->
      eprintf (f_"%s: could not find kernel log buffer in kernel image\n")
	domname

let summary = s_"display kernel messages"
let description = s_"\
virt-dmesg prints the kernel messages for virtual machines running
under libvirt.  The output is similar to the ordinary dmesg command
run inside the virtual machine."

let () = Virt_mem.register "dmesg" summary description ~run
