let warning =
  "This code is automatically generated from the kernel database by kerneldb-to-parser program.  Any edits you make will be lost.";;
let zero = 0;;
exception ParseError of string * string * string;;
type t = { net_device_dev_addr : string; net_device_name : string };;
let parser_1 bits = bitmatch bits with
  | { net_device_name : 128 : offset(0), string;
    net_device_dev_addr : 256 : offset(2240), string } -> { net_device_name = net_device_name;
    net_device_dev_addr = net_device_dev_addr }
  | { _ } -> raise (ParseError ("net_device", "parser_1", "failed to match kernel structure"));;
let parser_2 bits = bitmatch bits with
  | { net_device_name : 128 : offset(0), string;
    net_device_dev_addr : 256 : offset(2496), string } -> { net_device_name = net_device_name;
    net_device_dev_addr = net_device_dev_addr }
  | { _ } -> raise (ParseError ("net_device", "parser_2", "failed to match kernel structure"));;
let parser_3 bits = bitmatch bits with
  | { net_device_name : 128 : offset(0), string;
    net_device_dev_addr : 256 : offset(2688), string } -> { net_device_name = net_device_name;
    net_device_dev_addr = net_device_dev_addr }
  | { _ } -> raise (ParseError ("net_device", "parser_3", "failed to match kernel structure"));;
let parser_4 bits = bitmatch bits with
  | { net_device_name : 128 : offset(0), string;
    net_device_dev_addr : 256 : offset(4416), string } -> { net_device_name = net_device_name;
    net_device_dev_addr = net_device_dev_addr }
  | { _ } -> raise (ParseError ("net_device", "parser_4", "failed to match kernel structure"));;
let parser_5 bits = bitmatch bits with
  | { net_device_name : 128 : offset(0), string;
    net_device_dev_addr : 256 : offset(4416), string } -> { net_device_name = net_device_name;
    net_device_dev_addr = net_device_dev_addr }
  | { _ } -> raise (ParseError ("net_device", "parser_5", "failed to match kernel structure"));;
let parser_6 bits = bitmatch bits with
  | { net_device_name : 128 : offset(0), string;
    net_device_dev_addr : 256 : offset(3392), string } -> { net_device_name = net_device_name;
    net_device_dev_addr = net_device_dev_addr }
  | { _ } -> raise (ParseError ("net_device", "parser_6", "failed to match kernel structure"));;
let parser_7 bits = bitmatch bits with
  | { net_device_name : 128 : offset(0), string;
    net_device_dev_addr : 256 : offset(3264), string } -> { net_device_name = net_device_name;
    net_device_dev_addr = net_device_dev_addr }
  | { _ } -> raise (ParseError ("net_device", "parser_7", "failed to match kernel structure"));;
let parser_8 bits = bitmatch bits with
  | { net_device_name : 128 : offset(0), string;
    net_device_dev_addr : 256 : offset(4288), string } -> { net_device_name = net_device_name;
    net_device_dev_addr = net_device_dev_addr }
  | { _ } -> raise (ParseError ("net_device", "parser_8", "failed to match kernel structure"));;
let parser_9 bits = bitmatch bits with
  | { net_device_name : 128 : offset(0), string;
    net_device_dev_addr : 256 : offset(1728), string } -> { net_device_name = net_device_name;
    net_device_dev_addr = net_device_dev_addr }
  | { _ } -> raise (ParseError ("net_device", "parser_9", "failed to match kernel structure"));;
let parser_10 bits = bitmatch bits with
  | { net_device_name : 128 : offset(0), string;
    net_device_dev_addr : 256 : offset(1728), string } -> { net_device_name = net_device_name;
    net_device_dev_addr = net_device_dev_addr }
  | { _ } -> raise (ParseError ("net_device", "parser_10", "failed to match kernel structure"));;
let parser_11 bits = bitmatch bits with
  | { net_device_name : 128 : offset(0), string;
    net_device_dev_addr : 256 : offset(2528), string } -> { net_device_name = net_device_name;
    net_device_dev_addr = net_device_dev_addr }
  | { _ } -> raise (ParseError ("net_device", "parser_11", "failed to match kernel structure"));;
let parser_12 bits = bitmatch bits with
  | { net_device_name : 128 : offset(0), string;
    net_device_dev_addr : 256 : offset(4288), string } -> { net_device_name = net_device_name;
    net_device_dev_addr = net_device_dev_addr }
  | { _ } -> raise (ParseError ("net_device", "parser_12", "failed to match kernel structure"));;
let parser_13 bits = bitmatch bits with
  | { net_device_name : 128 : offset(0), string;
    net_device_dev_addr : 256 : offset(3392), string } -> { net_device_name = net_device_name;
    net_device_dev_addr = net_device_dev_addr }
  | { _ } -> raise (ParseError ("net_device", "parser_13", "failed to match kernel structure"));;
let parser_14 bits = bitmatch bits with
  | { net_device_name : 128 : offset(0), string;
    net_device_dev_addr : 256 : offset(2528), string } -> { net_device_name = net_device_name;
    net_device_dev_addr = net_device_dev_addr }
  | { _ } -> raise (ParseError ("net_device", "parser_14", "failed to match kernel structure"));;
let parser_15 bits = bitmatch bits with
  | { net_device_name : 128 : offset(0), string;
    net_device_dev_addr : 256 : offset(1696), string } -> { net_device_name = net_device_name;
    net_device_dev_addr = net_device_dev_addr }
  | { _ } -> raise (ParseError ("net_device", "parser_15", "failed to match kernel structure"));;
let parser_16 bits = bitmatch bits with
  | { net_device_name : 128 : offset(0), string;
    net_device_dev_addr : 256 : offset(2624), string } -> { net_device_name = net_device_name;
    net_device_dev_addr = net_device_dev_addr }
  | { _ } -> raise (ParseError ("net_device", "parser_16", "failed to match kernel structure"));;
let parser_17 bits = bitmatch bits with
  | { net_device_name : 128 : offset(0), string;
    net_device_dev_addr : 256 : offset(1696), string } -> { net_device_name = net_device_name;
    net_device_dev_addr = net_device_dev_addr }
  | { _ } -> raise (ParseError ("net_device", "parser_17", "failed to match kernel structure"));;
module StringMap = Map.Make(String);;
let map = StringMap.empty;;
let v = (parser_1, 1284);;
let map = StringMap.add "2.6.21-1.3128.fc7.i586" v map;;
let v = (parser_2, 1092);;
let map = StringMap.add "2.6.22-0.23.rc7.git6.fc8.ppc" v map;;
let v = (parser_1, 1284);;
let map = StringMap.add "2.6.21-1.3128.fc7.i686" v map;;
let v = (parser_1, 1284);;
let map = StringMap.add "2.6.21-1.3132.fc7.i686" v map;;
let v = (parser_2, 1092);;
let map = StringMap.add "2.6.21-1.3236.fc8.ppc" v map;;
let v = (parser_1, 1116);;
let map = StringMap.add "2.6.21-1.3228.fc7.i586" v map;;
let v = (parser_3, 1108);;
let map = StringMap.add "2.6.18-1.2798.fc6.x86_64" v map;;
let v = (parser_4, 2316);;
let map = StringMap.add "2.6.21-1.3236.fc8.x86_64" v map;;
let v = (parser_5, 2044);;
let map = StringMap.add "2.6.21-1.3218.fc8.ppc64" v map;;
let v = (parser_6, 1684);;
let map = StringMap.add "2.6.21-1.3228.fc7.ppc64" v map;;
let v = (parser_3, 1404);;
let map = StringMap.add "2.6.21-1.3228.fc7.x86_64" v map;;
let v = (parser_7, 1408);;
let map = StringMap.add "2.6.21-1.3218.fc8.i586" v map;;
let v = (parser_4, 2316);;
let map = StringMap.add "2.6.22-0.23.rc7.git6.fc8.x86_64" v map;;
let v = (parser_6, 1924);;
let map = StringMap.add "2.6.21-1.3144.fc7.ppc64" v map;;
let v = (parser_2, 1092);;
let map = StringMap.add "2.6.21-1.3218.fc8.ppc" v map;;
let v = (parser_5, 2044);;
let map = StringMap.add "2.6.22-0.23.rc7.git6.fc8.ppc64" v map;;
let v = (parser_7, 1408);;
let map = StringMap.add "2.6.21-1.3236.fc8.i686" v map;;
let v = (parser_6, 1924);;
let map = StringMap.add "2.6.21-1.3132.fc7.ppc64" v map;;
let v = (parser_1, 1116);;
let map = StringMap.add "2.6.21-1.3228.fc7.i686" v map;;
let v = (parser_8, 2288);;
let map = StringMap.add "2.6.24-9.fc9.x86_64" v map;;
let v = (parser_5, 2044);;
let map = StringMap.add "2.6.22-0.23.rc7.git6.fc8.ppc64" v map;;
let v = (parser_6, 1924);;
let map = StringMap.add "2.6.21-1.3132.fc7.ppc64" v map;;
let v = (parser_8, 2288);;
let map = StringMap.add "2.6.24-7.fc9.x86_64" v map;;
let v = (parser_1, 912);;
let map = StringMap.add "2.6.18-1.2798.fc6.i586" v map;;
let v = (parser_9, 1020);;
let map = StringMap.add "2.6.21-1.3132.fc7.ppc" v map;;
let v = (parser_5, 2044);;
let map = StringMap.add "2.6.21-1.3234.fc8.ppc64" v map;;
let v = (parser_1, 912);;
let map = StringMap.add "2.6.18-1.2798.fc6.i686" v map;;
let v = (parser_8, 1752);;
let map = StringMap.add "2.6.25.11-97.fc9.x86_64" v map;;
let v = (parser_10, 672);;
let map = StringMap.add "2.6.18-1.2798.fc6.i686" v map;;
let v = (parser_11, 1244);;
let map = StringMap.add "2.6.24-9.fc9.i686" v map;;
let v = (parser_12, 1984);;
let map = StringMap.add "2.6.24-7.fc9.ppc64" v map;;
let v = (parser_7, 1408);;
let map = StringMap.add "2.6.22-0.23.rc7.git6.fc8.i686" v map;;
let v = (parser_5, 2044);;
let map = StringMap.add "2.6.21-1.3236.fc8.ppc64" v map;;
let v = (parser_2, 1116);;
let map = StringMap.add "2.6.21-1.3209.fc8.ppc" v map;;
let v = (parser_5, 2044);;
let map = StringMap.add "2.6.21-1.3236.fc8.ppc64" v map;;
let v = (parser_6, 1684);;
let map = StringMap.add "2.6.21-1.3228.fc7.ppc64" v map;;
let v = (parser_6, 1924);;
let map = StringMap.add "2.6.21-1.3128.fc7.ppc64" v map;;
let v = (parser_7, 1408);;
let map = StringMap.add "2.6.21-1.3209.fc8.i686" v map;;
let v = (parser_13, 2196);;
let map = StringMap.add "2.6.21-1.3132.fc7.x86_64" v map;;
let v = (parser_12, 1984);;
let map = StringMap.add "2.6.24-7.fc9.ppc64" v map;;
let v = (parser_13, 2196);;
let map = StringMap.add "2.6.21-1.3228.fc7.x86_64" v map;;
let v = (parser_14, 1072);;
let map = StringMap.add "2.6.24-9.fc9.ppc" v map;;
let v = (parser_9, 672);;
let map = StringMap.add "2.6.18-1.2798.fc6.ppc" v map;;
let v = (parser_9, 1020);;
let map = StringMap.add "2.6.21-1.3128.fc7.ppc" v map;;
let v = (parser_7, 1408);;
let map = StringMap.add "2.6.21-1.3209.fc8.i686" v map;;
let v = (parser_13, 1540);;
let map = StringMap.add "2.6.20-1.2933.fc6.x86_64" v map;;
let v = (parser_7, 1408);;
let map = StringMap.add "2.6.21-1.3234.fc8.i686" v map;;
let v = (parser_9, 1020);;
let map = StringMap.add "2.6.21-1.3144.fc7.ppc" v map;;
let v = (parser_6, 1924);;
let map = StringMap.add "2.6.21-1.3144.fc7.ppc64" v map;;
let v = (parser_12, 1984);;
let map = StringMap.add "2.6.24-9.fc9.ppc64" v map;;
let v = (parser_2, 1116);;
let map = StringMap.add "2.6.21-1.3218.fc8.ppc" v map;;
let v = (parser_9, 996);;
let map = StringMap.add "2.6.21-1.3128.fc7.ppc" v map;;
let v = (parser_13, 1260);;
let map = StringMap.add "2.6.18-1.2798.fc6.x86_64" v map;;
let v = (parser_9, 996);;
let map = StringMap.add "2.6.21-1.3132.fc7.ppc" v map;;
let v = (parser_9, 936);;
let map = StringMap.add "2.6.21-1.3228.fc7.ppc" v map;;
let v = (parser_12, 1984);;
let map = StringMap.add "2.6.24-9.fc9.ppc64" v map;;
let v = (parser_5, 2044);;
let map = StringMap.add "2.6.21-1.3218.fc8.ppc64" v map;;
let v = (parser_6, 1260);;
let map = StringMap.add "2.6.18-1.2798.fc6.ppc64" v map;;
let v = (parser_3, 2124);;
let map = StringMap.add "2.6.21-1.3128.fc7.x86_64" v map;;
let v = (parser_1, 912);;
let map = StringMap.add "2.6.20-1.2933.fc6.i686" v map;;
let v = (parser_13, 2196);;
let map = StringMap.add "2.6.21-1.3144.fc7.x86_64" v map;;
let v = (parser_6, 1260);;
let map = StringMap.add "2.6.20-1.2933.fc6.ppc64" v map;;
let v = (parser_2, 1116);;
let map = StringMap.add "2.6.22-0.23.rc7.git6.fc8.ppc" v map;;
let v = (parser_14, 1048);;
let map = StringMap.add "2.6.24-7.fc9.ppc" v map;;
let v = (parser_2, 1092);;
let map = StringMap.add "2.6.21-1.3209.fc8.ppc" v map;;
let v = (parser_13, 1260);;
let map = StringMap.add "2.6.20-1.2933.fc6.x86_64" v map;;
let v = (parser_1, 1284);;
let map = StringMap.add "2.6.21-1.3132.fc7.i586" v map;;
let v = (parser_1, 1284);;
let map = StringMap.add "2.6.21-1.3144.fc7.i586" v map;;
let v = (parser_13, 1660);;
let map = StringMap.add "2.6.21-1.3228.fc7.x86_64" v map;;
let v = (parser_6, 1260);;
let map = StringMap.add "2.6.18-1.2798.fc6.ppc64iseries" v map;;
let v = (parser_11, 1244);;
let map = StringMap.add "2.6.24-7.fc9.i586" v map;;
let v = (parser_15, 668);;
let map = StringMap.add "2.6.20-1.2933.fc6.ppc" v map;;
let v = (parser_7, 1408);;
let map = StringMap.add "2.6.21-1.3218.fc8.i686" v map;;
let v = (parser_1, 1284);;
let map = StringMap.add "2.6.21-1.3144.fc7.i686" v map;;
let v = (parser_1, 1116);;
let map = StringMap.add "2.6.21-1.3228.fc7.i686" v map;;
let v = (parser_7, 1408);;
let map = StringMap.add "2.6.21-1.3209.fc8.i586" v map;;
let v = (parser_4, 2316);;
let map = StringMap.add "2.6.21-1.3218.fc8.x86_64" v map;;
let v = (parser_6, 1260);;
let map = StringMap.add "2.6.18-1.2798.fc6.ppc64iseries" v map;;
let v = (parser_1, 1284);;
let map = StringMap.add "2.6.21-1.3144.fc7.i686" v map;;
let v = (parser_16, 1100);;
let map = StringMap.add "2.6.20-1.2933.fc6.x86_64" v map;;
let v = (parser_2, 1116);;
let map = StringMap.add "2.6.21-1.3234.fc8.ppc" v map;;
let v = (parser_13, 1260);;
let map = StringMap.add "2.6.20-1.2933.fc6.x86_64" v map;;
let v = (parser_13, 1260);;
let map = StringMap.add "2.6.18-1.2798.fc6.x86_64" v map;;
let v = (parser_11, 1244);;
let map = StringMap.add "2.6.24-7.fc9.i686" v map;;
let v = (parser_3, 2124);;
let map = StringMap.add "2.6.21-1.3144.fc7.x86_64" v map;;
let v = (parser_9, 688);;
let map = StringMap.add "2.6.20-1.2933.fc6.ppc" v map;;
let v = (parser_11, 1244);;
let map = StringMap.add "2.6.24-7.fc9.i686" v map;;
let v = (parser_11, 1244);;
let map = StringMap.add "2.6.24-9.fc9.i686" v map;;
let v = (parser_1, 924);;
let map = StringMap.add "2.6.20-1.2933.fc6.i686" v map;;
let v = (parser_11, 1212);;
let map = StringMap.add "2.6.25.11-97.fc9.i686" v map;;
let v = (parser_1, 1284);;
let map = StringMap.add "2.6.21-1.3228.fc7.i686" v map;;
let v = (parser_1, 1284);;
let map = StringMap.add "2.6.21-1.3228.fc7.i686" v map;;
let v = (parser_1, 1284);;
let map = StringMap.add "2.6.21-1.3128.fc7.i686" v map;;
let v = (parser_9, 996);;
let map = StringMap.add "2.6.21-1.3144.fc7.ppc" v map;;
let v = (parser_1, 912);;
let map = StringMap.add "2.6.20-1.2933.fc6.i686" v map;;
let v = (parser_4, 2316);;
let map = StringMap.add "2.6.21-1.3209.fc8.x86_64" v map;;
let v = (parser_2, 1092);;
let map = StringMap.add "2.6.21-1.3238.fc8.ppc" v map;;
let v = (parser_4, 2316);;
let map = StringMap.add "2.6.21-1.3234.fc8.x86_64" v map;;
let v = (parser_2, 1092);;
let map = StringMap.add "2.6.21-1.3234.fc8.ppc" v map;;
let v = (parser_5, 2044);;
let map = StringMap.add "2.6.21-1.3234.fc8.ppc64" v map;;
let v = (parser_1, 912);;
let map = StringMap.add "2.6.20-1.2933.fc6.i586" v map;;
let v = (parser_3, 2124);;
let map = StringMap.add "2.6.21-1.3132.fc7.x86_64" v map;;
let v = (parser_1, 912);;
let map = StringMap.add "2.6.20-1.2933.fc6.i686" v map;;
let v = (parser_6, 1260);;
let map = StringMap.add "2.6.18-1.2798.fc6.ppc64" v map;;
let v = (parser_1, 912);;
let map = StringMap.add "2.6.18-1.2798.fc6.i686" v map;;
let v = (parser_9, 836);;
let map = StringMap.add "2.6.21-1.3228.fc7.ppc" v map;;
let v = (parser_7, 1408);;
let map = StringMap.add "2.6.21-1.3236.fc8.i686" v map;;
let v = (parser_2, 1116);;
let map = StringMap.add "2.6.21-1.3236.fc8.ppc" v map;;
let v = (parser_17, 668);;
let map = StringMap.add "2.6.20-1.2933.fc6.i686" v map;;
let v = (parser_7, 1408);;
let map = StringMap.add "2.6.21-1.3218.fc8.i686" v map;;
let v = (parser_11, 1244);;
let map = StringMap.add "2.6.24-9.fc9.i586" v map;;
let v = (parser_13, 2196);;
let map = StringMap.add "2.6.21-1.3128.fc7.x86_64" v map;;
let v = (parser_7, 1408);;
let map = StringMap.add "2.6.21-1.3234.fc8.i686" v map;;
let v = (parser_6, 1260);;
let map = StringMap.add "2.6.20-1.2933.fc6.ppc64" v map;;
let v = (parser_14, 1048);;
let map = StringMap.add "2.6.24-9.fc9.ppc" v map;;
let v = (parser_6, 1924);;
let map = StringMap.add "2.6.21-1.3128.fc7.ppc64" v map;;
let v = (parser_14, 1072);;
let map = StringMap.add "2.6.24-7.fc9.ppc" v map;;
let v = (parser_5, 2044);;
let map = StringMap.add "2.6.21-1.3209.fc8.ppc64" v map;;
let v = (parser_9, 688);;
let map = StringMap.add "2.6.18-1.2798.fc6.ppc" v map;;
let v = (parser_7, 1408);;
let map = StringMap.add "2.6.22-0.23.rc7.git6.fc8.i686" v map;;
let v = (parser_1, 924);;
let map = StringMap.add "2.6.20-1.2933.fc6.i686" v map;;
let v = (parser_5, 2044);;
let map = StringMap.add "2.6.21-1.3209.fc8.ppc64" v map;;
let v = (parser_1, 1284);;
let map = StringMap.add "2.6.21-1.3132.fc7.i686" v map;;
type kernel_version = string;;
let net_device_known version = StringMap.mem version map;;
let net_device_size version =
  let (_, size) = StringMap.find version map in size;;
let net_device_of_bits version bits =
  let (parsefn, _) = StringMap.find version map in parsefn bits;;
let get_net_device version mem addr =
  let (parsefn, size) = StringMap.find version map in
  let bytes = Virt_mem_mmap.get_bytes mem addr size in
  let bits = Bitstring.bitstring_of_string bytes in parsefn bits;;
