exception ParseError of string * string * string;;
type t = { net_device_dev_addr : string; net_device_name : string };;
type kernel_version = string;;
val net_device_known : kernel_version -> bool;;
val net_device_size : kernel_version -> int;;
val net_device_of_bits : kernel_version -> Bitstring.bitstring -> t;;
val get_net_device :
  kernel_version ->
    ('a, 'b, [ | `HasMapping ]) Virt_mem_mmap.t -> Virt_mem_mmap.addr -> t;;
