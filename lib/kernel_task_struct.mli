exception ParseError of string * string * string;;
type t =
  { task_struct_active_mm : Virt_mem_mmap.addr; task_struct_comm : string;
    task_struct_mm : Virt_mem_mmap.addr; task_struct_normal_prio : int64;
    task_struct_pid : int64; task_struct_prio : int64;
    task_struct_state : int64; task_struct_static_prio : int64;
    task_struct_tasks'next : Virt_mem_mmap.addr;
    task_struct_tasks'prev : Virt_mem_mmap.addr
  };;
type kernel_version = string;;
val task_struct_known : kernel_version -> bool;;
val task_struct_size : kernel_version -> int;;
val task_struct_of_bits : kernel_version -> Bitstring.bitstring -> t;;
val get_task_struct :
  kernel_version ->
    ('a, 'b, [ | `HasMapping ]) Virt_mem_mmap.t -> Virt_mem_mmap.addr -> t;;
