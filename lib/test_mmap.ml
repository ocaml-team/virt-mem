(* Test program for Virt_mem_mmap module.  Not for general consumption. *)

open Printf
open Virt_mem_mmap

let () =
  let mem = create () in
  let data = String.make 0x1000 '\001' in
  let mem = add_string mem data 0x800L in
  let data = String.make 0x1000 '\002' in
  let mem = add_string mem data 0x1000L in
  let data = String.make 0x1800 '\003' in
  let mem = add_string mem data 0L in
  let data = String.make 0x800 '\004' in
  let mem = add_string mem data 0x1000L in
  let data = String.make 0x800 '\005' in
  let mem = add_string mem data 0x2800L in
  let data = String.make 1 '\006' in
  let mem = add_string mem data 0L in
  let data = "hello, world!\000" in
  let mem = add_string mem data 0x20L in

  List.iter (
    fun addr ->
      try
	printf "byte @ %Lx = %d\n" addr (get_byte mem addr)
      with Invalid_argument "get_byte" ->
	printf "byte @ %Lx = HOLE\n" addr
  ) [ 0L; 0x1L;
      0x7ffL; 0x800L; 0x801L;
      0xfffL; 0x1000L; 0x1001L;
      0x17ffL; 0x1800L; 0x1801L;
      0x1fffL; 0x2000L; 0x2001L;
      0x27ffL; 0x2800L; 0x2801L;
      0x2fffL; 0x3000L; 0x3001L ];

  List.iter (
    fun addr ->
      try
	printf "three bytes @ %Lx = %S\n" addr (get_bytes mem addr 3)
      with
	Invalid_argument "get_bytes" ->
	  printf "three bytes @ %Lx found a HOLE\n" addr
  ) [ 0L; 0x1L; 0x7ffL; 0xfffL; 0x17ffL; 0x1fffL; 0x27ffL; 0x2800L;
      0x2ffdL; 0x2fffL ];

  printf "string @ 0x20 = %S\n" (get_string mem 0x20L)
