(** Memory info for virtual domains. *)
(* Memory info for virtual domains.
   (C) Copyright 2008 Richard W.M. Jones, Red Hat Inc.
   http://libvirt.org/

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)

val register :
  ?external_cmd:bool ->
  ?extra_args:(Arg.key * Arg.spec * Arg.doc) list ->
  ?argcheck:(bool -> unit) ->
  ?beforeksyms:(bool -> Virt_mem_types.image0 list -> unit) ->
  ?beforeutsname:(bool -> Virt_mem_types.image1 -> unit) ->
  ?run:(bool -> Virt_mem_types.image2 -> unit) ->
  string -> string -> Arg.usage_msg ->
  unit
  (** Tools register themselves with this call.

      The anonymous parameters are:
      - tool name (eg. "uname")
      - short summary
      - full usage message

      The optional callback functions are:
      - [?argcheck] called after arguments have been fully parsed
      so that the program can do any additional checks needed (eg.
      on [extra_args]),
      - [?beforeksyms] called after images are loaded and before
      kernel symbols are analyzed,
      - [?beforeutsname] called after kernel symbols are analyzed
      and before the kernel version is detected
      - [?run] called after everything
      (almost all tools supply this callback function).

      Pass [~external_cmd:false] if this tool doesn't have an
      external 'virt-tool' link.

      Pass [~extra_args:...] if this tool needs extra command
      line options.
  *)

val main : unit -> unit
  (** Entry point for the combined virt-mem executable. *)
