(* Memory info command for virtual domains.
   (C) Copyright 2008 Richard W.M. Jones, Red Hat Inc.
   http://libvirt.org/

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   Implements 'virt-mem capture' command.
 *)

open Printf
open ExtString

module D = Libvirt.Domain

open Virt_mem_types
open Virt_mem_gettext.Gettext

(* This will contain what is passed by the user as '-o' option. *)
let output_filename = ref ""

(* Early argument check. *)
let argcheck debug =
  (* -o flag must have been specified. *)
  let output_filename = !output_filename in
  if output_filename = "" then (
    prerr_endline (s_"virt-mem capture: '-o memoryimage' option is required");
    exit 1
  )

(* Capture the images before kernel symbol analysis is attempted.
 * Just save them to the output file(s).
 *)
let rec beforeksyms debug = function
  | [] ->
      prerr_endline
	(s_"virt-mem capture: warning: no kernel images were captured")
  | [image] ->
      (* Single image is saved to output_filename. *)
      save_image image !output_filename
  | images ->
      (* Multiple images are saved to output_filename.ID where ID
       * is the domain ID (if known) or a mangled domain name.
       *)
      List.iter (
	fun ({ dom = dom; domname = domname } as image) ->
	  let filename =
	    !output_filename ^ "." ^
	    match dom with
	    | Some dom -> string_of_int (D.get_id dom)
	    | None ->
		let f = function
		  | ('a'..'z'|'A'..'Z'|'0'..'9'|'_' as c) -> String.make 1 c
		  | _ -> ""
		in
		String.replace_chars f domname in
	  save_image image filename
      ) images

and save_image { domname = domname } filename =
  assert false;

  let chan = open_out filename in

  close_out chan;

  printf (f_"virt-mem capture: wrote kernel image from %s to filename %s\n")
    domname filename

let summary = s_"capture memory image for post-mortem analysis"
let description = s_"Capture a memory image to a file for later post-mortem
analysis.  Use the '-o memoryimage' option to specify the
output file.

Other tools can load the memory image using the '-t' option."

let extra_args = [
  "-o", Arg.Set_string output_filename,
    "memoryimage " ^s_"Set output filename"
]

let () =
  Virt_mem.register
    ~external_cmd:false ~extra_args
    ~argcheck ~beforeksyms
    "capture" summary description
