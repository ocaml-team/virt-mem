(** Common types. *)
(* Memory info command for virtual domains.
   (C) Copyright 2008 Richard W.M. Jones, Red Hat Inc.
   http://libvirt.org/

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   Common types.
 *)

(** {2 Kernel symbols} *)

type ksym = string
  (** A kernel symbol. *)

module Ksymmap : sig
  type key = String.t
  type 'a t = 'a Map.Make(String).t
  val empty : 'a t
  val is_empty : 'a t -> bool
  val add : key -> 'a -> 'a t -> 'a t
  val find : key -> 'a t -> 'a
  val remove : key -> 'a t -> 'a t
  val mem : key -> 'a t -> bool
  val iter : (key -> 'a -> unit) -> 'a t -> unit
  val map : ('a -> 'b) -> 'a t -> 'b t
  val mapi : (key -> 'a -> 'b) -> 'a t -> 'b t
  val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
  val compare : ('a -> 'a -> int) -> 'a t -> 'a t -> int
  val equal : ('a -> 'a -> bool) -> 'a t -> 'a t -> bool
end
  (** A map of kernel symbols to addresses. *)

(** {2 Kernel images and associated data} *)

type utsname = {
  kernel_name : string;
  nodename : string;
  kernel_release : string;
  kernel_version : string;
  machine : string;
  domainname : string;
}
  (** Kernel version, from utsname structure in the kernel. *)

type image0 = {
  dom : Libvirt.ro Libvirt.Domain.t option; (** Domain, if known. *)
  domname : string;			(** Domain name. *)
  arch : Virt_mem_utils.architecture;	(** Architecture, eg. i386. *)
  mem : ([`Wordsize], [`Endian], [`HasMapping]) Virt_mem_mmap.t;
                                        (** Memory map. *)
  kernel_min : Virt_mem_mmap.addr;	(** Minimum addr of kernel pointers. *)
  kernel_max : Virt_mem_mmap.addr;	(** Maximum addr of kernel pointers. *)
}
  (** A basic kernel image. *)

type image1 =
    image0
    * Virt_mem_mmap.addr Ksymmap.t	(* Kernel symbol map. *)
  (** A kernel image, after finding kernel symbols. *)

type image2 =
    image0
    * Virt_mem_mmap.addr Ksymmap.t	(* Kernel symbol map. *)
    * utsname option			(* Kernel version, etc., if found. *)
  (** A kernel image, after finding kernel version (like 'uname'). *)

(** {2 Load kernel memory} *)

type load_memory_error =
  | AddressOutOfRange		(** Address not in [kernel_min..kernel_max] *)
  | DomIsNull			(** image.dom = None *)

exception LoadMemoryError of load_memory_error * string

val load_static_memory : dom:Libvirt.ro Libvirt.Domain.t ->
  domname:string ->
  arch:Virt_mem_utils.architecture ->
  wordsize:Virt_mem_utils.wordsize -> endian:Bitstring.endian ->
  kernel_min:Virt_mem_mmap.addr -> kernel_max:Virt_mem_mmap.addr ->
  Virt_mem_mmap.addr -> int -> image0
  (** [load_static_memory ~dom (*...*) start size] creates an [image0]
      object, and initializes it with static kernel memory loaded
      from the [start] address and [size] of [dom].

      See also {!load_memory} for exceptions this can raise. *)

val load_memory : image0 -> Virt_mem_mmap.addr -> int -> image0
  (** [load_memory img start size] tries to load [size] bytes from
      the start address into the memory map.  If the memory was loaded
      previously, then it is not requested again.

      Note that the memory map may be updated by this, so a modified
      image structure is returned.

      This function can raise many different sorts of exceptions and
      the caller is advised to catch any exceptions and deal with them
      appropriately. *)
