(* Memory info for virtual domains.
   (C) Copyright 2008 Richard W.M. Jones, Red Hat Inc.
   http://libvirt.org/

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)

open Printf

open Virt_mem_gettext.Gettext
open Virt_mem_utils
open Virt_mem_types

let run debug ({ domname = domname }, _, utsname) =
  match utsname with
  | Some u ->
      printf "%s: %s %s %s %s %s %s\n"
	domname
	u.kernel_name u.nodename u.kernel_release
	u.kernel_version u.machine u.domainname
  | None ->
      eprintf (f_"%s: no system_utsname in kernel image\n") domname

let summary = s_"uname command for virtual machines"
let description = s_"\
virt-uname prints the uname information such as OS version,
architecture and node name for virtual machines running under
libvirt."

let () = Virt_mem.register "uname" summary description ~run
